export const environment = {
  production: true,
  brokerUrl: 'wss://test.mosquitto.org:8081',
  subTopics: {
    tempeature: 'temperature',
    time: 'time',
    state: 'state',
    control: 'control'
  },
  mainTopics: {
    refregerators: {
      ref1: 'Refrigerators/1',
      ref2: 'Refrigerators/2'
    }
  }
};
