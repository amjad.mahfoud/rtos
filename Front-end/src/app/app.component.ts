import { Component } from '@angular/core';
import { IClientOptions, MqttClient, connect } from 'mqtt';
import { TempretureValue } from './Modells/TempretureValue';
import { DialogService } from 'primeng/dynamicdialog';
import { ControlComponent } from './control/control.component';
import { Observable, interval } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { time } from 'console';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    DialogService
  ]
})
export class AppComponent {
  title = 'FrontEnd';
  firstTempretureLastFetched: Date = new Date();
  secondTempretureLastFetched: Date = new Date();
  tempretureValue1: TempretureValue = new TempretureValue(this.firstTempretureLastFetched.getSeconds().toString(), 0);
  tempretureValue2: TempretureValue = new TempretureValue(this.secondTempretureLastFetched.getSeconds().toString(), 0);
  client: MqttClient = connect(environment.brokerUrl);
  isClientConnected: boolean = false;
  isRefrigeratorsConnected: boolean[] =[true, true];
  firstTimeOffset: number = 5000;
  secondTimeOffset: number = 5000;

  calibrateTime = new Observable(() => {
    setInterval(() => {
      var newDate = new Date();
      this.firstTimeOffset = Math.abs(newDate.getSeconds() - this.firstTempretureLastFetched.getSeconds()) + 1;
      this.firstTimeOffset = this.firstTimeOffset * 1000;

      this.secondTimeOffset = Math.abs(newDate.getSeconds() - this.secondTempretureLastFetched.getSeconds()) + 1;
      this.secondTimeOffset = this.secondTimeOffset * 1000;
    },10000)
  });

  checkConnection = new Observable(() => {
    setInterval(() => {
      var newDate = new Date();
      if (((newDate.getSeconds() - this.firstTempretureLastFetched.getSeconds()) > 10 && newDate.getMinutes() == this.firstTempretureLastFetched.getMinutes() && newDate.getHours() == this.firstTempretureLastFetched.getHours()) || newDate.getMinutes() > this.firstTempretureLastFetched.getMinutes() + 1 || newDate.getHours() > this.firstTempretureLastFetched.getHours() + 1) {
        this.isRefrigeratorsConnected[0] = false;
      }
      else {
        this.isRefrigeratorsConnected[0] = true;
      }

      if (((newDate.getSeconds() - this.secondTempretureLastFetched.getSeconds()) > 10 && newDate.getMinutes() == this.secondTempretureLastFetched.getMinutes() && newDate.getHours() == this.secondTempretureLastFetched.getHours())|| newDate.getMinutes() > this.secondTempretureLastFetched.getMinutes() + 1 || newDate.getHours() > this.secondTempretureLastFetched.getHours() + 1) {
        this.isRefrigeratorsConnected[1] = false;
      }
      else {
        this.isRefrigeratorsConnected[1] = true;
      }
    }, 1000);
  });

  constructor(public dialogService: DialogService) {
    //subscribe to topics
    this.client.subscribe(`${environment.mainTopics.refregerators.ref1}/${environment.subTopics.tempeature}`);
    this.client.subscribe(`${environment.mainTopics.refregerators.ref2}/${environment.subTopics.tempeature}`);
    this.client.subscribe(`${environment.mainTopics.refregerators.ref1}/${environment.subTopics.time}`);
    this.client.subscribe(`${environment.mainTopics.refregerators.ref2}/${environment.subTopics.time}`);
    this.client.subscribe(`${environment.mainTopics.refregerators.ref1}/${environment.subTopics.state}`);
    this.client.subscribe(`${environment.mainTopics.refregerators.ref2}/${environment.subTopics.state}`);

    this.client.on('connect', () => {
      console.log('Connected');
      this.isClientConnected = true;
    });

    this.client.on('error', function (error: any) {
      console.log(error.message);
    });

    //handle messages
    this.client.on('message', (topic: any, message: any) => {
      if (topic == `${environment.mainTopics.refregerators.ref1}/${environment.subTopics.time}`) {
        this.firstTempretureLastFetched = new Date(Number(message.toString())*1000 + this.firstTimeOffset);
      }

      if (topic == `${environment.mainTopics.refregerators.ref1}/${environment.subTopics.tempeature}`) {

        this.tempretureValue1 = new TempretureValue(this.firstTempretureLastFetched.getSeconds().toString(), Number(Number(message.toString()).toFixed(2)));
      }

      if (topic == `${environment.mainTopics.refregerators.ref2}/${environment.subTopics.time}`) {
        this.secondTempretureLastFetched = new Date(Number(message.toString())*1000 + this.secondTimeOffset);
      }

      if (topic == `${environment.mainTopics.refregerators.ref2}/${environment.subTopics.tempeature}`) {
        this.tempretureValue2 = new TempretureValue(this.secondTempretureLastFetched.getSeconds().toString(), Number(Number(message.toString()).toFixed(2)));
      }
    });
    this.checkConnection.subscribe();
    this.calibrateTime.subscribe();
  }

  showControlPanel1(): void {
    const ref = this.dialogService.open(ControlComponent, {
      data: {
        tempretureValue: this.tempretureValue1,
        refrigerator: environment.mainTopics.refregerators.ref1
      },
      header: 'Control Panel',
      width: '70%',
      footer: '\t',
      dismissableMask: true,

    });
  }

  showControlPanel2(): void {
    const ref = this.dialogService.open(ControlComponent, {
      data: {
        tempretureValue: this.tempretureValue2,
        refrigerator: environment.mainTopics.refregerators.ref2
      },
      header: 'Control Panel',
      width: '70%',
      footer: '\t',
      dismissableMask: true,
    });
  }
}
