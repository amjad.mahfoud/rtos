import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphComponent } from './control/graph/graph.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from "primeng/dialog";
import { ControlComponent } from './control/control.component';
import { FormsModule } from '@angular/forms';
import {InputSwitchModule} from 'primeng/inputswitch';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    ControlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    InputSwitchModule,
    MatButtonModule,
    MatToolbarModule,
    DynamicDialogModule,
    MatSlideToggleModule,
    FormsModule,
    DialogModule,
    MatProgressSpinnerModule,
    MatIconModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
