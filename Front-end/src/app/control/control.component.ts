import { state } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { connect, MqttClient } from 'mqtt';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Observable } from 'rxjs';
import { TempretureValue } from '../Modells/TempretureValue';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {
  tempretureValue: TempretureValue;
  refrigerator: string;
  TempretureLastFetched: Date = new Date();
  tempretureStatus: string = "stable";
  timeOffset: number = 5000;

  calibrateTime = new Observable(() => {
    setInterval(() => {
      var newDate = new Date();
      this.timeOffset =Math.abs(newDate.getSeconds() - this.TempretureLastFetched.getSeconds()) + 1;
      this.timeOffset = this.timeOffset * 1000;
    },10000)
  });

  checkConnection = new Observable(() => {
    setInterval(() => {
      var newDate = new Date();
      if (((newDate.getSeconds() - this.TempretureLastFetched.getSeconds()) > 10 && newDate.getMinutes() == this.TempretureLastFetched.getMinutes() && newDate.getHours() == this.TempretureLastFetched.getHours())|| newDate.getMinutes() > this.TempretureLastFetched.getMinutes() + 1 || newDate.getHours() > this.TempretureLastFetched.getHours() + 1) {
        this.ref.close();
      }
    }, 1000);
  });

  client: MqttClient = connect(environment.brokerUrl);
  constructor(public config: DynamicDialogConfig, private ref: DynamicDialogRef) {
    this.tempretureValue = config.data.tempretureValue;
    this.refrigerator = config.data.refregerator;

    //subscribe to topics
    this.client.subscribe(`${this.refrigerator}/${environment.subTopics.tempeature}`);
    this.client.subscribe(`${this.refrigerator}/${environment.subTopics.time}`);
    this.client.subscribe(`${this.refrigerator}/${environment.subTopics.state}`);

    this.client.on('connect', function () {
      console.log('Connected');
    });

    this.client.on('error', function (error: any) {
      console.log(error.message);
    });

    //handle messages
    this.client.on('message', (topic: any, message: any) => {
      if (topic == `${this.refrigerator}/${environment.subTopics.time}`) {
        this.TempretureLastFetched = new Date(Number(message.toString()) * 1000 + this.timeOffset);
      }
      if (topic == `${this.refrigerator}/${environment.subTopics.tempeature}`) {
        this.tempretureValue = new TempretureValue(this.TempretureLastFetched.getSeconds().toString(), Number(Number(message.toString()).toFixed(2)));
      }
      else if (topic == `${this.refrigerator}/${environment.subTopics.state}`) {
        this.tempretureStatus = message.toString();
      }
    });
    this.checkConnection.subscribe();
    this.calibrateTime.subscribe();
  }

  ngOnInit(): void {
  }

  startCoolingClick() {
    this.client.publish(`${this.refrigerator}/${environment.subTopics.control}`, "cooling");
  }

  extremeCoolingClick() {
    this.client.publish(`${this.refrigerator}/${environment.subTopics.control}`, "extremeCooling");
  }

  stopCoolingClick() {
    this.client.publish(`${this.refrigerator}/${environment.subTopics.control}`, "stopCooling");
  }
}
