import { Component, Input, NgModule, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { TempretureValue } from '../../Modells/TempretureValue';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {
  timeArray: string[] = [];
  TempretureArray: number[] = [];
  chartOption: EChartsOption = {};
  color: string = "green";

  @Input() set tempretureStatus(tempretureStatus: string) {
    this.color = (tempretureStatus == 'stable') ? 'green' : (tempretureStatus == 'freezing') ? 'skyblue' : (tempretureStatus == 'error') ? 'red' : (tempretureStatus == 'high') ? 'orangered' : 'yellow';
  }

  @Input() set tempretureValue(tempretureValue: TempretureValue) {
    this.timeArray.push(tempretureValue.time);
    this.TempretureArray.push(tempretureValue.value);
    this.chartOption = {
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: this.timeArray,
        name: 'Time seconds'
      },
      yAxis: {
        type: 'value',
        name: 'Tempreture',
        axisLabel: {
          formatter: '{value} °C'
        }
      },
      tooltip: {
        trigger: 'axis',
        alwaysShowContent: true,
        formatter: function (params: any) {
          return `<b>Tempreture</b> : ${params[0].value} °C`;
        }
      },
      series: [{
        data: this.TempretureArray,
        type: 'line',
        color: this.color,

      }]
    }
  };
  constructor() {
    
  }

  ngOnInit(): void {
  }
}
