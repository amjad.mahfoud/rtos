export class TempretureValue {
    time: string;
    value: number;

    constructor(date: string, number: number){
        this.time = date;
        this.value = number;
    }
}