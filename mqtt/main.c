#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>

#include "definitions.h"

static int mid_sent = 0;
static int qos = 0;
static int retain = 0;

double getTime()
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    return now.tv_sec + now.tv_nsec * 1e-9;
}

float *temp_value, *default_increment_value;

char *getRefStatues(float temp)
{
    if (temp > -10)
        return MQTT_STATE_ERROR;
    if (temp < -10 && temp >= -15)
        return MQTT_STATE_HOT;
    if (temp < -15 && temp >= -20)
        return MQTT_STATE_HIGH;
    if (temp < -20 && temp >= -25)
        return MQTT_STATE_STABLE;
    return MQTT_STATE_FREEZING;
}

void on_connect(struct mosquitto *mosq, void *obj, int rc)
{
    printf("ID: %d\n", *(int *)obj);
    if (rc)
    {
        printf("Error with result code: %d\n", rc);
        exit(-1);
    }
    mosquitto_subscribe(mosq, NULL, MQTT_TOPIC_REFRIGERATOR1_CONTROL, 0);
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg)
{
    printf("New message with topic %s: %s\n", msg->topic, (char *)msg->payload);
    // cooling, extremcooling, stop colling
    char *control = (char *)msg->payload;

    if (strcmp(control, MQTT_CONTROL_COOLING) == 0)
    {
        *default_increment_value = -0.4;
    }
    else if (strcmp(control, MQTT_CONTROL_EXTREM_COOLING) == 0)
    {
        // do something else
        *default_increment_value = -0.4;
    }
    else /* default: */
    {
        *default_increment_value = 0.1;
    }
}

int main()
{
    temp_value = (float *)mmap(NULL, sizeof(float), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    default_increment_value = (float *)mmap(NULL, sizeof(float), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    // setting up shared variables
    *temp_value = -20;
    *default_increment_value = 0.1;
    printf("- Starting tempreature: %f \n", *temp_value);
    printf("-     Default in value: %f \n", *default_increment_value);

    // forks make a copy of variables
    pid_t child = fork();
    if (child < 0)
    {
        perror("The fork calling was not succesful\n");
        exit(1);
    }

    // handle main logic
    if (child > 0) // the parent process, it can see the returning value of fork - the child variable!
    {
        int rc;

        struct mosquitto *mosq;

        mosquitto_lib_init();
        mosq = mosquitto_new("temp_value_publisher_rtos", true, NULL);

        // mosquitto_tls_opts_set(mosq,
        //                        0,
        //                        "tlv1.2",
        //                        NULL);

        rc = mosquitto_connect(mosq, MQTT_HOSTNAME, MQTT_PORT, 60);
        if (rc != 0)
        {
            printf("Client could not connect to broker! Error Code: %d\n", rc);
            mosquitto_destroy(mosq);
            return -1;
        }
        printf("We are now connected to the broker!\n");

        while (1)
        {
            *temp_value += *default_increment_value;
            if (*temp_value <= -30)
            {
                *default_increment_value = 0.1;
            }
            // publish temp
            char tempreature[10];
            sprintf(tempreature, "%f", *temp_value);
            char *state = getRefStatues(*temp_value);

            mosquitto_publish(mosq, NULL, MQTT_TOPIC_REFRIGERATOR1_STATE, strlen(state), state, 0, false);
            mosquitto_publish(mosq, NULL, MQTT_TOPIC_REFRIGERATOR1_TEMPERATURE, strlen(tempreature), tempreature, QOS, RETAIN);
            printf("we published %s: %s\n", MQTT_TOPIC_REFRIGERATOR1_STATE, state);
            printf("we published %s: %s\n", MQTT_TOPIC_REFRIGERATOR1_TEMPERATURE, tempreature);

            double tom = getTime();
            char tomStr[10];
            sprintf(tomStr, "%f", tom);

            mosquitto_publish(mosq, NULL, MQTT_TOPIC_REFRIGERATOR1_TIME, strlen(tomStr), tomStr, QOS, RETAIN);
            printf("we published %s: %s\n", MQTT_TOPIC_REFRIGERATOR1_TIME, tomStr);
            sleep(1);
        }
        mosquitto_disconnect(mosq);
        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
    }
    else // child process, this should listen and change value
    {
        int rc;

        mosquitto_lib_init();

        struct mosquitto *mosq;

        mosq = mosquitto_new("temp_value_control_rtos", true, NULL);
        mosquitto_connect_callback_set(mosq, on_connect);
        mosquitto_message_callback_set(mosq, on_message);
        // mosquitto_tls_opts_set(mosq,
        //                        0,
        //                        "tlv1.2",
        //                        NULL);

        rc = mosquitto_connect(mosq, MQTT_HOSTNAME, MQTT_PORT, 10);
        if (rc)
        {
            printf("Could not connect to Broker with return code %d\n", rc);
            return -1;
        }

        mosquitto_loop_forever(mosq, -1, 1);
        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
    }
    return 0;
}