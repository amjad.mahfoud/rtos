// #define MQTT_HOSTNAME "localhost"
#define MQTT_HOSTNAME "test.mosquitto.org"
#define MQTT_PORT 1883
#define MQTT_CLIENTID "g8_rtos_asf"

// topics
#define MQTT_TOPIC_REFRIGERATOR1_TEMPERATURE "Refrigerators/1/temperature"
#define MQTT_TOPIC_REFRIGERATOR1_CONTROL "Refrigerators/1/control"
#define MQTT_TOPIC_REFRIGERATOR1_STATE "Refrigerators/1/state"
#define MQTT_TOPIC_REFRIGERATOR1_TIME "Refrigerators/1/time"

#define MQTT_TOPIC_REFRIGERATOR2_TEMPERATURE "Refrigerators/2/temperature"
#define MQTT_TOPIC_REFRIGERATOR2_CONTROL "Refrigerators/2/control"
#define MQTT_TOPIC_REFRIGERATOR2_STATE "Refrigerators/2/state"
#define MQTT_TOPIC_REFRIGERATOR2_TIME "Refrigerators/2/time"

// states
#define MQTT_STATE_STABLE "stable"
#define MQTT_STATE_ERROR "error"
#define MQTT_STATE_FREEZING "freezing"
#define MQTT_STATE_HIGH "high"
#define MQTT_STATE_HOT "hot"

// controls
#define MQTT_CONTROL_COOLING "cooling"
#define MQTT_CONTROL_EXTREM_COOLING "extremeCooling"
#define MQTT_CONTROL_STOP_COOLING "stopCooling"

// mqtt
#define QOS 0
#define RETAIN 0