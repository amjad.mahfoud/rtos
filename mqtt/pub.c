#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

#include "definitions.h"

static int mid_sent = 0;
static int qos = 0;
static int retain = 0;
static float temp = -20;

void my_disconnect_callback(struct mosquitto *mosq, void *obj, int rc){
    // printf("Disconnected!\n");
}

void my_publish_callback(struct mosquitto *mosq, void *obj, int mid){
    // printf("Published!\n");
}

char* getRefStatues(){
    if(temp > -15)
        return MQTT_STATE_NOT;
    if(temp< -20)
        return MQTT_STATE_STABLE;
    return MQTT_STATE_INCREASING;
}


int main(int argc, char *argv[])
{
    struct mosquitto *mosq = NULL;

    mosquitto_lib_init();
    mosq = mosquitto_new(MQTT_CLIENTID, true, NULL);
    if (!mosq)
    {
        printf("Cant initiallize mosquitto library\n");
        exit(-1);
    }
    mosquitto_disconnect_callback_set(mosq, my_disconnect_callback);
    mosquitto_publish_callback_set(mosq, my_publish_callback);
    int rc = mosquitto_connect(mosq, MQTT_HOSTNAME, MQTT_PORT, 60);
    if (rc != 0)
    {
        printf("Client could not connect to broker! Error Code: %d\n", rc);
        mosquitto_destroy(mosq);
        exit(0);
    }
    
    printf("We are now connected to the broker!\n");

    while(1){
        temp += 0.1;
        char* state = getRefStatues();
        // publish state
        mosquitto_publish(mosq, &mid_sent, MQTT_TOPIC_REFRIGERATOR1_STATE, strlen(state), state, qos, retain);
        printf("we published %s: %s with mid: %d\n", MQTT_TOPIC_REFRIGERATOR1_STATE, state, mid_sent);
        // publish temp
        char tempreature[10];
        sprintf(tempreature, "%f", temp);
        mosquitto_publish(mosq, &mid_sent, MQTT_TOPIC_REFRIGERATOR1_TEMPERATURE, strlen(tempreature), tempreature, qos, retain);
        printf("we published %s: %s with mid: %d\n", MQTT_TOPIC_REFRIGERATOR1_TEMPERATURE, tempreature, mid_sent);
        sleep(1);
    }

    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();

    return 0;
}